﻿using UnityEngine;
using System.Collections;

public class ButtonCtrl : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void EnterAgain()
    {
        GameObject list = GameObject.Find("ScrollView");
        list.transform.position = new Vector3(-50, list.transform.position.y, list.transform.position.z);
        iTween.MoveTo(list, iTween.Hash("x", 0, "easeType", iTween.EaseType.easeInOutQuart, "delay", .1f, "time", 1, "oncomplete", "moveToComplete"));
    }
}
