﻿using UnityEngine;
using System.Collections;

public class ItemModel : MonoBehaviour {

    public string title, url;

    
    public ItemModel(string title, string url)
    {
        this.title = title;
        this.url = url;
    }
    
}
