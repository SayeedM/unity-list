﻿using UnityEngine;
using System.Collections;

public class ListController : MonoBehaviour {

    

    
    public GameObject ContentPanel;
    public GameObject ListItemPrefab;


    private string[] titles =
    {
        "Google",
        "Youtube",
        "Facebook",
        "Animation Tool",
        "Unity3D",
        "Unity3D Tutorials",
        "Modern GUI Development",
        "PDF Sample",
        "Uni Web View"
    };


    private string[] urls =
    {
        "http://www.google.com",
        "https://www.youtube.com",
        "https://www.facebook.com",
        "https://matthewlein.com/ceaser/",
        "https://unity3d.com/",
        "https://unity3d.com/learn",
        "https://www.youtube.com/watch?v=QxRAIjXdfFU",
        "http://liris.cnrs.fr/Documents/Liris-1297.pdf",
        "https://github.com/onevcat/UniWebView"
    };



    ArrayList items;

    void Start()
    {


        // 1. Get the data to be displayed
        items = new ArrayList();

        for (int i = 0; i < titles.Length; i++)
        {
            ItemModel model = new ItemModel(titles[i], urls[i]);
            items.Add(model);
        }

        
        
    

        // 2. Iterate through the data, 
        //	  instantiate prefab, 
        //	  set the data, 
        //	  add it to panel
        foreach (ItemModel item in items)
        {
            GameObject newAnimal = Instantiate(ListItemPrefab) as GameObject;
            ItemController controller = newAnimal.GetComponent<ItemController>();
            controller.title.text = item.title;
            controller.data = item;
            //controller.url.text = item.url;
            newAnimal.transform.parent = ContentPanel.transform;
            newAnimal.transform.localScale = Vector3.one;
        }
    }
    
}
