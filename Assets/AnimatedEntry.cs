﻿using UnityEngine;
using System.Collections;

public class AnimatedEntry : MonoBehaviour {

	// Use this for initialization
	void Start () {
        iTween.MoveTo(gameObject, iTween.Hash("x", 0, "easeType", iTween.EaseType.easeInOutQuart, "delay", .1f, "time", 1, "oncomplete", "moveToComplete"));
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
